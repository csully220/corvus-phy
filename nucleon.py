class Nucleon:

    def __init__(self, x=0, y=0, radius=1):
        self.x = x
        self.y = y
        self.radius = radius

    def __str__(self):
        return 'Nucleon, [{self.x},{self.y}]'.format(self=self)

class Quaternion:
    def __init__(self, a=0, b=0, c=0, d=0):
        self.a=a
        self.b=b
        self.c=c
        self.d=d

    def __string(self):
        return '{self.a} + {self.b} + {self.c} + {self.d}'.format(self=self)

class Vector:
    def __init__(self, x=0, y=0, z=0):
        self.x=x
        self.y=y
        self.z=z

    def __string(self):
        return '{self.x} + {self.y} + {self.z}'.format(self=self)
