from tkinter import Tk, W, E, filedialog, StringVar, LabelFrame, Listbox, Menu
from tkinter.ttk import Frame, Button, Entry, Style, Label
import os, stat, ntpath


class Example(Frame):

    # top level directory containing PDAL
    
    
    def __init__(self): 
        super().__init__() 
        self.initUI()
 
    def initUI(self):
        self.master.title("Corvus Physics")

        # Menu Bar
        menu = Menu(self.master)
        self.master.config(menu=menu)

        # File Menu
        file = Menu(menu, tearoff=0)
        file.add_command(label="Reset")
        # add File Menu to Menu Bar
        menu.add_cascade(label="File", menu=file)

        # Style and padding
        Style().configure("TButton", padding=(3, 5, 3, 5), font='serif 10')
        self.columnconfigure(0, pad=3) 
        self.rowconfigure(0, pad=3)

        grp_quat = LabelFrame(self.master, text="Quaternion")
        grp_quat.grid(row=0, column=0)
        grp_quat.config(padx=50, pady=20)

        ent_a = Entry(grp_quat)
        ent_a.grid(row=0, column=0)
        ent_b = Entry(grp_quat)
        ent_b.grid(row=1, column=0)
        ent_c = Entry(grp_quat)
        ent_c.grid(row=2, column=0)
        ent_d = Entry(grp_quat)
        ent_d.grid(row=3, column=0)
        btn_save_quat = Button(grp_quat, text="Enter")
        btn_save_quat.grid(row=4, column=0)

def main():
    app = Example()
    app.mainloop()

main()
